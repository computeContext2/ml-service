package ru.ifmo.mlservice.v1.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.SpringValidatorAdapter;
import ru.ifmo.mlservice.v1.resources.DataSet;

@Component
public class DataSetValidator implements Validator {

    private static final String F_ATTR = "attributes";
    private static final String F_TRAIN = "train";
    private static final String F_TEST = "test";

    private static final String E_NOT_BLANK = "NotBlank";
    private static final String E_NOT_MATCH = "NotMatch";

    private final SpringValidatorAdapter validatorAdapter;

    @Autowired
    DataSetValidator(SpringValidatorAdapter validatorAdapter) {
        this.validatorAdapter = validatorAdapter;
    }

    @Override
    public boolean supports(@NonNull Class<?> clazz) {
        return DataSet.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(@NonNull Object target, @NonNull Errors errors) {
        DataSet dataSet = (DataSet) target;

        // JSR 303 validation
        if (hasErrors(dataSet)) {
            return;
        }

        validateAttributes(dataSet, errors);
        validateData(dataSet, errors);
    }

    private boolean hasErrors(DataSet dataSet) {
        Errors errors = new BeanPropertyBindingResult(dataSet, "dataSet");
        validatorAdapter.validate(dataSet, errors);
        return errors.hasErrors();
    }

    private void validateAttributes(DataSet dataSet, Errors errors) {
        for (String attr : dataSet.getAttributes()) {
            if (attr == null || attr.isEmpty()) {
                errors.rejectValue(F_ATTR, E_NOT_BLANK,
                        "The attribute name must not be blank.");
            }
        }
    }

    private void validateData(DataSet dataSet, Errors errors) {
        for (double[] values : dataSet.getTrain()) {
            if (values.length != dataSet.getAttributes().size()) {
                errors.rejectValue(F_TRAIN, E_NOT_MATCH,
                        "The instance does not match the attribute definition.");
            }
        }
        if (dataSet.getTest() == null) {
            return;
        }
        for (double[] values : dataSet.getTest()) {
            if (values.length != dataSet.getAttributes().size()) {
                errors.rejectValue(F_TEST, E_NOT_MATCH,
                        "The instance does not match the attribute definition.");
            }
        }

    }
}

