package ru.ifmo.mlservice.v1.services.factories;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ifmo.mlservice.v1.models.AbstractModel;
import ru.ifmo.mlservice.v1.models.DataSet;
import weka.classifiers.AbstractClassifier;
import weka.classifiers.Evaluation;
import weka.core.Instances;

import java.util.UUID;
import java.util.function.Supplier;

@Component
public class ModelFactory<T extends AbstractModel, C extends AbstractClassifier> {

    private final InstancesFactory instancesFactory;

    private Supplier<T> factory;
    private Supplier<C> classifierFactory;

    @Autowired
    ModelFactory(InstancesFactory instancesFactory) {
        this.instancesFactory = instancesFactory;
    }

    private Supplier<T> getFactory() {
        if (factory == null) {
            throw new IllegalStateException("Set the model factory first.");
        }
        return factory;
    }

    public void setFactory(Supplier<T> factory) {
        this.factory = factory;
    }

    private Supplier<C> getClassifierFactory() {
        if (classifierFactory == null) {
            throw new IllegalStateException("Set the classifier factory first.");
        }
        return classifierFactory;
    }

    public void setClassifierFactory(Supplier<C> classifierFactory) {
        this.classifierFactory = classifierFactory;
    }

    private T create(String tag, AbstractClassifier classifier, Evaluation evaluation) {
        T model = getFactory().get();
        model.setId(UUID.randomUUID());
        model.setTag(tag);
        model.setModel(classifier);
        model.setEvaluation(evaluation);
        return model;
    }

    private T create(String tag, Instances train, Instances test) throws Exception {
        AbstractClassifier classifier = getClassifierFactory().get();
        classifier.buildClassifier(train);
        if (test == null) {
            return create(tag, classifier, null);
        }
        Evaluation evaluation = new Evaluation(train);
        evaluation.evaluateModel(classifier, test);
        return create(tag, classifier, evaluation);
    }

    public T create(@NonNull String tag, @NonNull String trainResource,
                    String testResource, int classIndex) throws Exception {
        Instances train = instancesFactory.fromResource(trainResource, classIndex);
        Instances test = null;
        if (testResource != null) {
            test = instancesFactory.fromResource(testResource, classIndex);
        }
        return create(tag, train, test);
    }

    public T create(@NonNull DataSet dataSet) throws Exception {
        Instances train = instancesFactory.fromDataSet(dataSet, false);
        Instances test = null;
        if (dataSet.getTest() != null) {
            test = instancesFactory.fromDataSet(dataSet, true);
        }
        return create(dataSet.getName(), train, test);
    }
}
