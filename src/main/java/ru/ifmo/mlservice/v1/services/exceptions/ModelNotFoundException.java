package ru.ifmo.mlservice.v1.services.exceptions;

import ru.ifmo.mlservice.v1.services.exceptions.commons.ModelException;

public class ModelNotFoundException extends ModelException {

    public ModelNotFoundException() {
        super("Model not found.");
    }
}
