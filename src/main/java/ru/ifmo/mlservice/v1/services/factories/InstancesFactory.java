package ru.ifmo.mlservice.v1.services.factories;

import lombok.NonNull;
import org.springframework.stereotype.Component;
import ru.ifmo.mlservice.ServiceApplication;
import ru.ifmo.mlservice.v1.models.DataSet;
import weka.core.Instances;
import weka.core.converters.ArffLoader;

import java.io.IOException;

@Component
class InstancesFactory {

    Instances fromResource(@NonNull String name, int classIndex) throws IOException {
        ArffLoader loader = new ArffLoader();
        loader.setSource(ServiceApplication.class.getResourceAsStream(name));
        Instances instances = loader.getDataSet();
        instances.setClassIndex(classIndex);
        return instances;
    }

    Instances fromDataSet(@NonNull DataSet dataSet, boolean test) {
        DataSetLoader loader = new DataSetLoader();
        loader.setSource(dataSet);
        Instances instances = loader.getDataSet(test);
        instances.setClassIndex(dataSet.getAttributes().size() - 1);
        return instances;
    }
}
