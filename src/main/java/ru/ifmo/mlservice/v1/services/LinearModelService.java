package ru.ifmo.mlservice.v1.services;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.stereotype.Service;
import ru.ifmo.mlservice.v1.models.DataSet;
import ru.ifmo.mlservice.v1.models.LinearModel;
import ru.ifmo.mlservice.v1.repositories.LinearModelRepository;
import ru.ifmo.mlservice.v1.services.exceptions.ModelConflictException;
import ru.ifmo.mlservice.v1.services.exceptions.ModelNotFoundException;
import ru.ifmo.mlservice.v1.services.exceptions.commons.ModelException;
import ru.ifmo.mlservice.v1.services.factories.ModelFactory;
import weka.classifiers.Classifier;
import weka.classifiers.functions.LinearRegression;
import weka.core.DenseInstance;

import javax.annotation.PostConstruct;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@Service
@Slf4j
public class LinearModelService {

    private final ModelFactory<LinearModel, LinearRegression> modelFactory;
    private final LinearModelRepository modelRepository;

    @Autowired
    public LinearModelService(ModelFactory<LinearModel, LinearRegression> modelFactory, LinearModelRepository modelRepository) {
        this.modelFactory = modelFactory;
        this.modelRepository = modelRepository;
    }

    @PostConstruct
    @SuppressWarnings("unused")
    void init() {
        modelFactory.setClassifierFactory(LinearRegression::new);
        modelFactory.setFactory(LinearModel::new);
    }

    private LinearModel buildModel(DataSet dataSet) {
        try {
            return modelFactory.create(dataSet);
        } catch (Exception e) {
            throw new ModelException("Model creation error.", e);
        }
    }

    private LinearModel buildModel(String tag, String trainResource, String testResource, int classIndex) {
        try {
            return modelFactory.create(tag, trainResource, testResource, classIndex);
        } catch (Exception e) {
            throw new ModelException("Model creation error.", e);
        }
    }

    public LinearModel createModel(@NonNull DataSet dataSet) {
        return modelRepository.save(buildModel(dataSet));
    }

    public List<Double> predict(@NonNull UUID id, @NonNull List<double[]> values) {
        LinearModel model = modelRepository.findById(id)
                .orElseThrow(ModelNotFoundException::new);

        List<Double> prediction = new LinkedList<>();
        Classifier classifier = model.getModel();
        for (double[] factor : values) {
            double value;
            try {
                value = classifier.classifyInstance(new DenseInstance(1.0d, factor));
            } catch (Exception e) {
                throw new ModelException("Instance classification error.", e);
            }
            prediction.add(value);
        }
        return prediction;
    }

    public List<LinearModel> listModels() {
        return modelRepository.findAll();
    }

    public LinearModel getModel(@NonNull UUID id) {
        return modelRepository.findById(id)
                .orElseThrow(ModelNotFoundException::new);
    }

    public LinearModel updateModel(@NonNull UUID id, @NonNull DataSet dataSet) {
        return modelRepository.findById(id)
                .map(oldModel -> {
                    try {
                        return modelRepository.save(oldModel.patch(buildModel(dataSet)));
                    } catch (OptimisticLockingFailureException e) {
                        throw new ModelConflictException(e);
                    }
                })
                .orElseThrow(ModelNotFoundException::new);
    }

    public LinearModel restoreModel(@NonNull UUID id, @NonNull String tag, @NonNull String trainResource,
                                    @NonNull String testResource, int classIndex) {
        LinearModel model = buildModel(tag, trainResource, testResource, classIndex);
        return modelRepository.findById(id)
                .map(oldModel -> {
                    try {
                        return modelRepository.save(oldModel.patch(model));
                    } catch (OptimisticLockingFailureException e) {
                        log.warn("Model was updated by another transaction.", e);
                        return model;
                    }
                })
                .orElseGet(() -> {
                    model.setId(id);
                    return modelRepository.save(model);
                });
    }

    public void deleteModel(@NonNull UUID id) {
        modelRepository.findById(id).ifPresent(model -> {
            try {
                modelRepository.deleteById(id);
            } catch (EmptyResultDataAccessException e) {
                log.warn("Model was deleted by another transaction.", e);
            }
        });
    }
}
