package ru.ifmo.mlservice.v1.services.factories;

import ru.ifmo.mlservice.v1.models.DataSet;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instances;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

class DataSetLoader {

    private DataSet dataSet;

    void setSource(DataSet dataSet) {
        this.dataSet = dataSet;
    }

    private Instances getStructure() {
        if (dataSet == null) {
            throw new UnsupportedOperationException("The data set is not defined.");
        }
        ArrayList<Attribute> attributes = dataSet.getAttributes().stream()
                .map(Attribute::new)
                .collect(Collectors.toCollection(ArrayList::new));
        return new Instances(dataSet.getName(), attributes, 0);
    }

    Instances getDataSet(boolean test) {
        Instances instances = getStructure();

        List<double[]> data;
        if (test) {
            data = dataSet.getTest();
            if (data == null) {
                throw new UnsupportedOperationException("The data set does not contain test data.");
            }
        } else {
            data = dataSet.getTrain();
        }

        data.forEach(values -> instances.add(new DenseInstance(1.0d, values)));
        return instances;
    }
}
