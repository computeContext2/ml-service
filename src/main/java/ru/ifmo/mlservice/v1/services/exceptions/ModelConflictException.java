package ru.ifmo.mlservice.v1.services.exceptions;

import ru.ifmo.mlservice.v1.services.exceptions.commons.ModelException;

public class ModelConflictException extends ModelException {

    public ModelConflictException(Throwable cause) {
        super("Concurrent operation failed.", cause);
    }
}
