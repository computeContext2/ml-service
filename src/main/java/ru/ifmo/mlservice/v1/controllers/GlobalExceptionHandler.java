package ru.ifmo.mlservice.v1.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.ifmo.mlservice.v1.services.exceptions.ModelConflictException;
import ru.ifmo.mlservice.v1.services.exceptions.ModelNotFoundException;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ControllerAdvice(basePackages = "ru.ifmo.mlservice.v1.controllers")
public class GlobalExceptionHandler {

    @ExceptionHandler(ModelNotFoundException.class)
    @SuppressWarnings("unused")
    public void handleModelNotFoundException(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.NOT_FOUND.value());
    }

    @ExceptionHandler(ModelConflictException.class)
    @SuppressWarnings("unused")
    public void handleModelConflictException(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.CONFLICT.value());
    }
}

