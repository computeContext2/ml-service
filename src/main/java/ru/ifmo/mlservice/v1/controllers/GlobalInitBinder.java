package ru.ifmo.mlservice.v1.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;
import ru.ifmo.mlservice.v1.validators.DataSetValidator;

@ControllerAdvice(basePackages = "ru.ifmo.mlservice.v1.controllers")
public class GlobalInitBinder {

    private final DataSetValidator dataSetValidator;

    @Autowired
    GlobalInitBinder(DataSetValidator dataSetValidator) {
        this.dataSetValidator = dataSetValidator;
    }

    @InitBinder("dataSet")
    public void initBinder(WebDataBinder binder) {
        binder.addValidators(dataSetValidator);
    }
}
