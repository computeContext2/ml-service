package ru.ifmo.mlservice.v1.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.ifmo.mlservice.v1.models.LinearModel;
import ru.ifmo.mlservice.v1.resources.DataSet;
import ru.ifmo.mlservice.v1.resources.LinearModelResource;
import ru.ifmo.mlservice.v1.resources.assemblers.LinearModelResourceAssembler;
import ru.ifmo.mlservice.v1.resources.mappers.DataSetMapper;
import ru.ifmo.mlservice.v1.services.LinearModelService;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;

import static ru.ifmo.mlservice.v1.Api.API_BASE;

/**
 * Represents the application programming interface for working with a linear regression.
 */
@RestController
@RequestMapping(LinearModelController.CONTROLLER_BASE)
@CrossOrigin
@Api(tags = {"Linear Model"})
public class LinearModelController {

    static final String CONTROLLER_BASE = API_BASE + "/linear_model";

    private final LinearModelService modelService;
    private final DataSetMapper dataSetMapper;
    private final LinearModelResourceAssembler modelResourceAssembler;

    @Autowired
    public LinearModelController(LinearModelService modelService, DataSetMapper dataSetMapper,
                                 LinearModelResourceAssembler modelResourceAssembler) {
        this.modelService = modelService;
        this.dataSetMapper = dataSetMapper;
        this.modelResourceAssembler = modelResourceAssembler;
    }

    /**
     * Creates a new linear regression model.
     *
     * @param dataSet training data and optional test data
     * @return a new model that was created
     * @throws URISyntaxException ...
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "createModel", notes = "Creates a new linear regression model.")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<LinearModelResource> createModel(@Valid @RequestBody DataSet dataSet) throws URISyntaxException {
        LinearModel model = modelService.createModel(dataSetMapper.toDataSet(dataSet));
        LinearModelResource resource = modelResourceAssembler.toResource(model);
        return ResponseEntity
                .created(new URI(resource.getId().expand().getHref()))
                .body(resource);
    }

    /**
     * Predicts values using a given linear regression model.
     *
     * @param id        unique model identifier
     * @param newValues new values of independent variables
     * @return a list of computed dependent variables
     */
    @PostMapping("/{id}/predict")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "predict", notes = "Predicts values using a given linear regression model.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<Double>> predict(@PathVariable UUID id, @NotEmpty @RequestBody List<double[]> newValues) {
        List<Double> predicted = modelService.predict(id, newValues);
        return ResponseEntity.ok(predicted);
    }

    /**
     * Gets the list of stored linear regression models.
     *
     * @return the list of stored linear regression models
     */
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "listModels", notes = "Gets the list of stored linear regression models.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<Resources<LinearModelResource>> listModels() {
        List<LinearModel> models = modelService.listModels();
        Resources<LinearModelResource> resources = modelResourceAssembler.toResources(models);
        return ResponseEntity.ok(resources);
    }

    /**
     * Gets the specified linear regression model.
     *
     * @param id unique model identifier
     * @return the requested model
     */
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "getModel", notes = "Gets the specified linear regression model.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<LinearModelResource> getModel(@PathVariable UUID id) {
        LinearModel model = modelService.getModel(id);
        LinearModelResource resource = modelResourceAssembler.toResource(model);
        return ResponseEntity.ok(resource);
    }

    /**
     * Updates the specified linear regression model with new data.
     *
     * @param id      unique model identifier
     * @param dataSet training data and optional test data
     * @return the updated model
     */
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "updateModel", notes = "Updates the specified linear regression model with new data.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 409, message = "Conflict"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<LinearModelResource> updateModel(@PathVariable UUID id, @Valid @RequestBody DataSet dataSet) {
        LinearModel model = modelService.updateModel(id, dataSetMapper.toDataSet(dataSet));
        LinearModelResource resource = modelResourceAssembler.toResource(model);
        return ResponseEntity.ok(resource);
    }

    /**
     * Deletes the specified linear regression model.
     *
     * @param id unique model identifier
     * @return void
     */
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(value = "deleteModel", notes = "Deletes the specified linear regression model.")
    @ApiResponses({
            @ApiResponse(code = 204, message = "No Content"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<Object> deleteModel(@PathVariable UUID id) {
        modelService.deleteModel(id);
        return ResponseEntity.noContent().build();
    }
}