package ru.ifmo.mlservice.v1.models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Entity;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@ToString
public class LinearModel extends AbstractModel<LinearModel> {

}
