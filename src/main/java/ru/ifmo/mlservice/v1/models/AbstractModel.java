package ru.ifmo.mlservice.v1.models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.UpdateTimestamp;
import weka.classifiers.AbstractClassifier;
import weka.classifiers.Evaluation;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.UUID;

@MappedSuperclass
@Data
@EqualsAndHashCode
@ToString
public abstract class AbstractModel<T extends AbstractModel> {

    @Id
    @Type(type = "uuid-char")
    @Column(length = 36)
    protected UUID id;

    @Version
    @Column(nullable = false)
    protected Long version;

    @CreationTimestamp
    @Column(nullable = false, updatable = false)
    protected OffsetDateTime created;

    @UpdateTimestamp
    @Column(nullable = false)
    protected OffsetDateTime updated;

    @Column(length = 50, nullable = false)
    protected String tag;

    @Lob
    @Column(nullable = false)
    private AbstractClassifier model;

    @Lob
    private Evaluation evaluation;

    public T patch(@NonNull T other) {
        setTag(other.getTag());
        setModel(other.getModel());
        setEvaluation(other.getEvaluation());
        // noinspection unchecked
        return (T) this;
    }
}
