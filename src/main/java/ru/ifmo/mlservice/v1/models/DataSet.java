package ru.ifmo.mlservice.v1.models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;
import java.util.Set;

@Data
@EqualsAndHashCode
@ToString
public class DataSet {

    private String name;

    private Set<String> attributes;

    private List<double[]> train;

    private List<double[]> test;
}
