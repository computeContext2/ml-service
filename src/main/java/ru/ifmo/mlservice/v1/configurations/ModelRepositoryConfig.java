package ru.ifmo.mlservice.v1.configurations;

import lombok.Generated;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.ifmo.mlservice.v1.services.LinearModelService;

import java.util.UUID;

@Configuration
@Generated
@Slf4j
public class ModelRepositoryConfig {

    @Bean
    public CommandLineRunner initLinearModelRepository(LinearModelService modelService) {
        return args -> {
            UUID id = UUID.fromString("00000000-0000-0000-0000-000000000000");
            log.info("Pre-defined linear model {}.", modelService.restoreModel(id, "ice_cream_sales",
                    "/model/linear-train.arff", "/model/linear-test.arff", 1));
        };
    }
}