package ru.ifmo.mlservice.v1.resources;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Data
@EqualsAndHashCode
@ToString
@ApiModel(description = "Data set for training and evaluation")
public class DataSet {

    @NotBlank
    @ApiModelProperty("The name of the relation")
    private String name;

    @Size(min = 2, max = 1000)
    @ApiModelProperty("Names of all relation attributes")
    @JsonDeserialize(as = LinkedHashSet.class)
    private Set<String> attributes;

    @Size(min = 5)
    @ApiModelProperty("Training data")
    private List<double[]> train;

    @ApiModelProperty("Optional test data")
    private List<double[]> test;
}
