package ru.ifmo.mlservice.v1.resources.assemblers;

import org.springframework.hateoas.Resources;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;
import ru.ifmo.mlservice.v1.controllers.LinearModelController;
import ru.ifmo.mlservice.v1.models.LinearModel;
import ru.ifmo.mlservice.v1.resources.LinearModelResource;
import weka.classifiers.Evaluation;
import weka.classifiers.functions.LinearRegression;

import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class LinearModelResourceAssembler extends ResourceAssemblerSupport<LinearModel, LinearModelResource> {

    LinearModelResourceAssembler() {
        super(LinearModelController.class, LinearModelResource.class);
    }

    @Override
    public LinearModelResource toResource(LinearModel entity) {
        LinearModelResource resource = super.createResourceWithId(entity.getId(), entity);
        resource.add(linkTo(methodOn(LinearModelController.class).listModels()).withRel("all"));

        resource.setUuid(entity.getId());
        resource.setCreated(entity.getCreated());
        resource.setUpdated(entity.getUpdated());
        resource.setTag(entity.getTag());

        LinearRegression model = (LinearRegression) entity.getModel();
        resource.setCoefficients(model.coefficients());
        resource.setDescription(model.toString());

        Evaluation evaluation = entity.getEvaluation();
        if (evaluation != null) {
            resource.setEvaluationSummary(evaluation.toSummaryString(true));
        }

        return resource;
    }

    public Resources<LinearModelResource> toResources(List<LinearModel> entities) {
        return new Resources<>(super.toResources(entities),
                linkTo(methodOn(LinearModelController.class).listModels()).withSelfRel());
    }
}