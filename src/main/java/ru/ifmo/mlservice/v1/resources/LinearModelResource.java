package ru.ifmo.mlservice.v1.resources;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;

import java.time.OffsetDateTime;
import java.util.UUID;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString
@Relation(value = "linearModel", collectionRelation = "all")
@ApiModel(description = "Linear regression model")
public class LinearModelResource extends ResourceSupport {

    @ApiModelProperty("Unique model identifier")
    private UUID uuid;

    @ApiModelProperty("Date and time the model was created")
    private OffsetDateTime created;

    @ApiModelProperty("Date and time the model was last updated")
    private OffsetDateTime updated;

    @ApiModelProperty("Model tag")
    private String tag;

    @ApiModelProperty("Computed model coefficients")
    private double[] coefficients;

    @ApiModelProperty("Model description")
    private String description;

    @ApiModelProperty("Model summary")
    private String evaluationSummary;
}
