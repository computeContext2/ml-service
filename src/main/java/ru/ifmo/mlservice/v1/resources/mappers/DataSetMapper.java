package ru.ifmo.mlservice.v1.resources.mappers;

import lombok.NonNull;
import org.springframework.stereotype.Component;
import ru.ifmo.mlservice.v1.resources.DataSet;

@Component
public class DataSetMapper {

    public ru.ifmo.mlservice.v1.models.DataSet toDataSet(@NonNull DataSet dataSet) {
        ru.ifmo.mlservice.v1.models.DataSet model = new ru.ifmo.mlservice.v1.models.DataSet();

        model.setName(dataSet.getName());
        model.setAttributes(dataSet.getAttributes());
        model.setTrain(dataSet.getTrain());
        model.setTest(dataSet.getTest());

        return model;
    }
}
