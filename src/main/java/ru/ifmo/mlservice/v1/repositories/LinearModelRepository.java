package ru.ifmo.mlservice.v1.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.ifmo.mlservice.v1.models.LinearModel;

import java.util.UUID;

@Repository
public interface LinearModelRepository extends JpaRepository<LinearModel, UUID> {

}