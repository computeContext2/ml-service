package ru.ifmo.mlservice.v1.resources.assemblers;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resources;
import ru.ifmo.mlservice.v1.models.LinearModel;
import ru.ifmo.mlservice.v1.resources.LinearModelResource;
import weka.classifiers.Evaluation;
import weka.classifiers.functions.LinearRegression;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class LinearModelResourceAssemblerTest {

    @InjectMocks
    private LinearModelResourceAssembler modelResourceAssembler;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void linksAreValid() {
        // arrange
        LinearModel model = mock(LinearModel.class);
        when(model.getId()).thenReturn(UUID.randomUUID());
        when(model.getCreated()).thenReturn(OffsetDateTime.now());
        when(model.getUpdated()).thenReturn(OffsetDateTime.now());
        when(model.getModel()).thenReturn(mock(LinearRegression.class));

        // act
        LinearModelResource resource = modelResourceAssembler.toResource(model);

        // assert
        assertNotNull(resource);
        assertTrue(resource.hasLink(Link.REL_SELF));
    }

    @Test
    public void resourcesLinksAreValid() {
        // arrange
        LinearModel model = mock(LinearModel.class);
        when(model.getId()).thenReturn(UUID.randomUUID());
        when(model.getCreated()).thenReturn(OffsetDateTime.now());
        when(model.getUpdated()).thenReturn(OffsetDateTime.now());
        when(model.getModel()).thenReturn(mock(LinearRegression.class));

        List<LinearModel> models = Stream.of(model).collect(Collectors.toList());

        // act
        Resources<LinearModelResource> resources = modelResourceAssembler.toResources(models);

        // assert
        assertNotNull(resources);
        assertTrue(resources.hasLink(Link.REL_SELF));
    }

    @Test
    public void fieldsAreValid() {
        // arrange
        LinearRegression linearRegression = mock(LinearRegression.class);
        when(linearRegression.coefficients()).thenReturn(new double[]{1, 2, 3});
        when(linearRegression.toString()).thenReturn("Model description");

        Evaluation evaluation = mock(Evaluation.class);
        when(evaluation.toSummaryString(true)).thenReturn("Model complex summary");

        LinearModel model = mock(LinearModel.class);
        when(model.getId()).thenReturn(UUID.fromString("07156d94-998a-11e9-a2a3-2a2ae2dbcce4"));
        when(model.getCreated()).thenReturn(OffsetDateTime.parse("2001-03-25T15:43:22+03:00"));
        when(model.getUpdated()).thenReturn(OffsetDateTime.parse("2005-12-14T09:07:06+03:00"));
        when(model.getModel()).thenReturn(linearRegression);
        when(model.getEvaluation()).thenReturn(evaluation);

        // act
        LinearModelResource resource = modelResourceAssembler.toResource(model);

        // assert
        assertNotNull(resource);
        assertEquals(UUID.fromString("07156d94-998a-11e9-a2a3-2a2ae2dbcce4"), resource.getUuid());
        assertEquals(OffsetDateTime.parse("2001-03-25T15:43:22+03:00"), resource.getCreated());
        assertEquals(OffsetDateTime.parse("2005-12-14T09:07:06+03:00"), resource.getUpdated());
        assertArrayEquals(new double[]{1, 2, 3}, resource.getCoefficients(), 0.0);
        assertEquals("Model description", resource.getDescription());
        assertEquals("Model complex summary", resource.getEvaluationSummary());
    }

    @Test
    public void evalFieldsAreNull() {
        // arrange
        LinearModel model = mock(LinearModel.class);
        when(model.getId()).thenReturn(UUID.randomUUID());
        when(model.getCreated()).thenReturn(OffsetDateTime.now());
        when(model.getUpdated()).thenReturn(OffsetDateTime.now());
        when(model.getModel()).thenReturn(mock(LinearRegression.class));

        // act
        LinearModelResource resource = modelResourceAssembler.toResource(model);

        // assert
        assertNotNull(resource);
        assertNull(resource.getEvaluationSummary());
    }
}