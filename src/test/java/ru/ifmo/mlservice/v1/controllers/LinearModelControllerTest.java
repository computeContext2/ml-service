package ru.ifmo.mlservice.v1.controllers;

import com.google.common.net.HttpHeaders;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resources;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.ifmo.mlservice.v1.resources.LinearModelResource;
import ru.ifmo.mlservice.v1.resources.assemblers.LinearModelResourceAssembler;
import ru.ifmo.mlservice.v1.resources.mappers.DataSetMapper;
import ru.ifmo.mlservice.v1.services.LinearModelService;
import ru.ifmo.mlservice.v1.validators.DataSetValidator;

import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(LinearModelController.class)
public class LinearModelControllerTest {

    private static final String testDataSet = "{\"name\":\"test\"," +
            "\"attributes\":[\"factor\",\"label\"]," +
            "\"train\":[[0.0,0.0],[0.0,0.0],[0.0,0.0],[0.0,0.0],[0.0,0.0]]}";
    private static final UUID testModelId = UUID.randomUUID();

    @MockBean
    @SuppressWarnings("unused")
    private DataSetValidator dataSetValidator;
    @MockBean
    @SuppressWarnings("unused")
    private LinearModelService modelService;
    @MockBean
    @SuppressWarnings("unused")
    private DataSetMapper dataSetMapper;
    @MockBean
    @SuppressWarnings("unused")
    private LinearModelResourceAssembler modelResourceAssembler;

    @Autowired
    @SuppressWarnings("unused")
    private MockMvc mockMvc;

    private LinearModelResource resource;

    @Before
    public void setUp() {
        when(dataSetValidator.supports(any())).thenReturn(true);

        resource = new LinearModelResource();
        resource.setUuid(UUID.randomUUID());
        resource.add(new Link("http://localhost" + LinearModelController.CONTROLLER_BASE + "/" + resource.getUuid(), Link.REL_SELF));
        when(modelResourceAssembler.toResource(any())).thenReturn(resource);
        when(modelResourceAssembler.toResources(any())).thenReturn(
                new Resources<>(Stream.of(resource).collect(Collectors.toList())));
    }

    @Test
    public void createModel() throws Exception {
        mockMvc.perform(post(LinearModelController.CONTROLLER_BASE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(testDataSet))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(header().string(HttpHeaders.LOCATION, resource.getId().expand().getHref()))
                .andExpect(jsonPath("$._links.self.href", is(resource.getId().expand().getHref())));
    }

    @Test
    public void predict() throws Exception {
        mockMvc.perform(post(LinearModelController.CONTROLLER_BASE + "/{id}/predict", testModelId)
                .contentType(MediaType.APPLICATION_JSON)
                .content("[[1],[2],[3]]"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void listModels() throws Exception {
        mockMvc.perform(get(LinearModelController.CONTROLLER_BASE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded.all[0].uuid", is(resource.getUuid().toString())));
    }

    @Test
    public void getModel() throws Exception {
        mockMvc.perform(get(LinearModelController.CONTROLLER_BASE + "/{id}", testModelId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.uuid", is(resource.getUuid().toString())));
    }

    @Test
    public void updateModel() throws Exception {
        mockMvc.perform(put(LinearModelController.CONTROLLER_BASE + "/{id}", testModelId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(testDataSet))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.uuid", is(resource.getUuid().toString())));
    }

    @Test
    public void deleteModel() throws Exception {
        mockMvc.perform(delete(LinearModelController.CONTROLLER_BASE + "/{id}", testModelId))
                .andDo(print())
                .andExpect(status().isNoContent());
    }
}