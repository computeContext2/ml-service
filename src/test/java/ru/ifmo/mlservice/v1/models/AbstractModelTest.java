package ru.ifmo.mlservice.v1.models;

import org.junit.Test;
import weka.classifiers.AbstractClassifier;
import weka.classifiers.Evaluation;

import java.util.UUID;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class AbstractModelTest {

    @Test
    public void patch() {
        // arrange
        AbstractModel model = spy(new AbstractModel() {
            @Override
            public UUID getId() {
                return UUID.randomUUID();
            }
        });
        AbstractModel other = mock(AbstractModel.class);

        AbstractClassifier classifier = mock(AbstractClassifier.class);
        Evaluation evaluation = mock(Evaluation.class);
        when(other.getId()).thenReturn(UUID.randomUUID());
        when(other.getTag()).thenReturn("tag");
        when(other.getModel()).thenReturn(classifier);
        when(other.getEvaluation()).thenReturn(evaluation);

        // act
        // noinspection unchecked
        AbstractModel retVal = model.patch(other);

        // assert
        assertSame(model, retVal);
        assertEquals(other.getTag(), model.getTag());
        assertEquals(other.getModel(), model.getModel());
        assertEquals(other.getEvaluation(), model.getEvaluation());
        assertNotEquals(other.getId(), model.getId());
        verify(model, never()).setId(any());
    }
}