package ru.ifmo.mlservice.v1.services.factories;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import ru.ifmo.mlservice.v1.models.AbstractModel;
import ru.ifmo.mlservice.v1.models.DataSet;
import weka.classifiers.AbstractClassifier;

import java.util.HashSet;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class ModelFactoryTest {

    @Mock
    private InstancesFactory instancesFactory;
    @InjectMocks
    private ModelFactory modelFactory;

    @Before
    public void setUp() {
        initMocks(this);
        // noinspection unchecked
        modelFactory.setClassifierFactory(() -> mock(AbstractClassifier.class));
        // noinspection unchecked
        modelFactory.setFactory(() -> mock(AbstractModel.class));
    }

    @Test
    public void fromTrainResource() throws Exception {
        // arrange

        // act
        AbstractModel model = modelFactory.create("tag", "train", null, 1);

        // assert
        assertNotNull(model);
        verify(instancesFactory, times(1)).fromResource(eq("train"), eq(1));
    }

    @Test
    public void fromTrain() throws Exception {
        // arrange
        DataSet dataSet = mock(DataSet.class);
        when(dataSet.getAttributes()).thenReturn(new HashSet<>() {{
            add("column1");
            add("column2");
            add("column3");
        }});
        when(dataSet.getName()).thenReturn("relation");
        when(dataSet.getTest()).thenReturn(null);

        // act
        AbstractModel model = modelFactory.create(dataSet);

        // assert
        assertNotNull(model);
        verify(instancesFactory, times(1)).fromDataSet(refEq(dataSet), eq(false));
    }
}