package ru.ifmo.mlservice.v1.services;

import org.hamcrest.FeatureMatcher;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import ru.ifmo.mlservice.v1.models.DataSet;
import ru.ifmo.mlservice.v1.models.LinearModel;
import ru.ifmo.mlservice.v1.repositories.LinearModelRepository;
import ru.ifmo.mlservice.v1.services.factories.ModelFactory;
import weka.classifiers.AbstractClassifier;
import weka.classifiers.functions.LinearRegression;
import weka.core.Instance;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.everyItem;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class LinearModelServiceTest {

    @Mock
    private ModelFactory<LinearModel, LinearRegression> modelFactory;
    @Mock
    private LinearModelRepository modelRepository;
    @InjectMocks
    private LinearModelService linearModelService;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void createModel() throws Exception {
        // arrange
        DataSet dataSet = mock(DataSet.class);
        LinearModel model = mock(LinearModel.class);
        when(modelFactory.create(any(DataSet.class))).thenReturn(model);
        when(modelRepository.save(model)).thenReturn(model);

        // act
        LinearModel created = linearModelService.createModel(dataSet);

        // assert
        assertSame(model, created);
    }

    @Test
    public void predict() throws Exception {
        // arrange
        UUID id = UUID.randomUUID();
        AbstractClassifier classifier = mock(AbstractClassifier.class);
        LinearModel model = mock(LinearModel.class);
        when(model.getModel()).thenReturn(classifier);
        when(modelRepository.findById(id)).thenReturn(Optional.of(model));

        // act
        linearModelService.predict(id, new ArrayList<>() {{
            add(new double[]{1.0, 2.0});
            add(new double[]{3.0, 4.0});
            add(new double[]{5.0, 6.0});
        }});

        // assert
        ArgumentCaptor<Instance> argumentCaptor = ArgumentCaptor.forClass(Instance.class);
        verify(classifier, times(3)).classifyInstance(argumentCaptor.capture());
        List<Instance> instances = argumentCaptor.getAllValues();
        assertThat(instances, everyItem(
                new FeatureMatcher<>(is(1.0), "weight", "weight") {
                    @Override
                    protected Double featureValueOf(Instance actual) {
                        return actual.weight();
                    }
                }));
    }

    @Test
    public void listModels() {
        // arrange
        LinearModel model = mock(LinearModel.class);
        when(modelRepository.findAll()).thenReturn(Stream.of(model).collect(Collectors.toList()));

        // act
        List<LinearModel> models = linearModelService.listModels();

        // assert
        assertNotNull(models);
    }

    @Test
    public void getModel() {
        // arrange
        UUID id = UUID.randomUUID();
        LinearModel model = mock(LinearModel.class);
        when(modelRepository.findById(id)).thenReturn(Optional.of(model));

        // act
        LinearModel existing = linearModelService.getModel(id);

        // assert
        assertSame(model, existing);
    }

    @Test
    public void updateModel() throws Exception {
        // arrange
        UUID id = UUID.randomUUID();
        DataSet dataSet = mock(DataSet.class);
        LinearModel model = mock(LinearModel.class);
        when(model.patch(any())).thenReturn(model);
        when(modelFactory.create(any(DataSet.class))).thenReturn(model);
        when(modelRepository.findById(id)).thenReturn(Optional.of(model));
        when(modelRepository.save(model)).thenReturn(model);

        // act
        LinearModel updated = linearModelService.updateModel(id, dataSet);

        // assert
        assertSame(model, updated);
    }

    @Test
    public void deleteModel() {
        // arrange
        UUID id = UUID.randomUUID();
        LinearModel model = mock(LinearModel.class);
        when(modelRepository.findById(id)).thenReturn(Optional.of(model));

        // act
        linearModelService.deleteModel(id);

        // assert
        verify(modelRepository, times(1)).deleteById(eq(id));
    }
}