package ru.ifmo.mlservice.v1.services.factories;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import ru.ifmo.mlservice.v1.models.DataSet;
import weka.core.Instances;

import java.util.HashSet;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class InstancesFactoryTest {

    @InjectMocks
    private InstancesFactory instancesFactory;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void fromResource() throws Exception {
        // act
        Instances instances = instancesFactory.fromResource("/model/linear-train.arff", 1);

        // assert
        assertNotNull(instances);
    }

    @Test(expected = NullPointerException.class)
    public void fromMissingResource() throws Exception {
        // act
        instancesFactory.fromResource("/model/00000000000000000000", 1);
    }

    @Test
    public void fromDataSet() {
        // arrange
        DataSet dataSet = mock(DataSet.class);
        when(dataSet.getAttributes()).thenReturn(new HashSet<>() {{
            add("column1");
            add("column2");
            add("column3");
        }});

        // act
        Instances instances = instancesFactory.fromDataSet(dataSet, false);

        // assert
        assertNotNull(instances);
    }

    @Test(expected = NullPointerException.class)
    public void fromNullDataSet() {
        // act
        instancesFactory.fromDataSet(null, false);
    }
}