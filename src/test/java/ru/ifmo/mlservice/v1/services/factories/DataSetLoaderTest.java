package ru.ifmo.mlservice.v1.services.factories;

import org.hamcrest.FeatureMatcher;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Spy;
import ru.ifmo.mlservice.v1.models.DataSet;
import weka.core.Instance;
import weka.core.Instances;

import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.everyItem;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class DataSetLoaderTest {

    @Mock
    private DataSet dataSet;
    @Spy
    private DataSetLoader loader;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void sourceNotSet() {
        // act
        loader.getDataSet(false);
    }

    @Test
    public void trainIsValid() {
        // arrange
        when(dataSet.getTrain()).thenReturn(new ArrayList<>() {{
            add(new double[]{1.0, 2.0});
            add(new double[]{3.0, 4.0});
            add(new double[]{5.0, 6.0});
        }});
        loader.setSource(dataSet);

        // act
        Instances dataSet = loader.getDataSet(false);

        // assert
        assertEquals(1.0, dataSet.get(0).value(0), 0.0);
        assertEquals(2.0, dataSet.get(0).value(1), 0.0);
        assertEquals(3.0, dataSet.get(1).value(0), 0.0);
        assertEquals(4.0, dataSet.get(1).value(1), 0.0);
        assertEquals(5.0, dataSet.get(2).value(0), 0.0);
        assertEquals(6.0, dataSet.get(2).value(1), 0.0);
    }

    @Test
    public void trainWeight() {
        // arrange
        when(dataSet.getTrain()).thenReturn(new ArrayList<>() {{
            add(new double[]{1.0, 2.0});
            add(new double[]{3.0, 4.0});
            add(new double[]{5.0, 6.0});
        }});
        loader.setSource(dataSet);

        // act
        Instances dataSet = loader.getDataSet(false);

        // assert
        assertThat(dataSet, everyItem(
                new FeatureMatcher<>(is(1.0), "weight", "weight") {
                    @Override
                    protected Double featureValueOf(Instance actual) {
                        return actual.weight();
                    }
                }));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testIsNotPresent() {
        // arrange
        when(dataSet.getTest()).thenReturn(null);
        loader.setSource(dataSet);

        // act
        loader.getDataSet(true);
    }

    @Test
    public void testIsValid() {
        // arrange
        when(dataSet.getTest()).thenReturn(new ArrayList<>() {{
            add(new double[]{1.0, 2.0});
            add(new double[]{3.0, 4.0});
            add(new double[]{5.0, 6.0});
        }});
        loader.setSource(dataSet);

        // act
        Instances dataSet = loader.getDataSet(true);

        // assert
        assertEquals(1.0, dataSet.get(0).value(0), 0.0);
        assertEquals(2.0, dataSet.get(0).value(1), 0.0);
        assertEquals(3.0, dataSet.get(1).value(0), 0.0);
        assertEquals(4.0, dataSet.get(1).value(1), 0.0);
        assertEquals(5.0, dataSet.get(2).value(0), 0.0);
        assertEquals(6.0, dataSet.get(2).value(1), 0.0);
    }
}