package ru.ifmo.mlservice.v1.validators;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.beanvalidation.SpringValidatorAdapter;
import ru.ifmo.mlservice.v1.resources.DataSet;

import java.util.ArrayList;
import java.util.HashSet;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class DataSetValidatorTest {

    @Mock
    private DataSet target;
    @Mock
    private BeanPropertyBindingResult errors;
    @Mock
    private SpringValidatorAdapter validatorAdapter;
    @InjectMocks
    private DataSetValidator dataSetValidator;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void supportsTest() {
        // act
        boolean yes = dataSetValidator.supports(DataSet.class);
        boolean no = dataSetValidator.supports(Object.class);

        // assert
        assertTrue(yes);
        assertFalse(no);
    }

    @Test
    public void validatorAdapterUsage() {
        // act
        dataSetValidator.validate(target, errors);

        // assert
        verify(validatorAdapter, never()).validate(eq(target), eq(errors));
        // noinspection ConstantConditions
        verify(validatorAdapter, times(1)).validate(eq(target), not(eq(errors)));
    }

    @Test
    public void trainSize() {
        // arrange
        when(target.getAttributes()).thenReturn(new HashSet<>() {{
            add("column1");
            add("column2");
        }});
        when(target.getTrain()).thenReturn(new ArrayList<>() {{
            add(new double[2]);
            add(new double[1]); // error
            add(new double[2]);
        }});

        // act
        dataSetValidator.validate(target, errors);

        // assert
        verify(errors, times(1)).rejectValue(eq("train"), anyString(), anyString());
    }

    @Test
    public void testSize() {
        // arrange
        when(target.getAttributes()).thenReturn(new HashSet<>() {{
            add("column1");
            add("column2");
        }});
        when(target.getTest()).thenReturn(new ArrayList<>() {{
            add(new double[2]);
            add(new double[1]); // error
            add(new double[2]);
        }});

        // act
        dataSetValidator.validate(target, errors);

        // assert
        verify(errors, times(1)).rejectValue(eq("test"), anyString(), anyString());
    }

    @Test
    public void testIsNotPresent() {
        // arrange
        when(target.getAttributes()).thenReturn(new HashSet<>() {{
            add("column1");
            add("column2");
        }});
        when(target.getTest()).thenReturn(null);

        // act
        dataSetValidator.validate(target, errors);

        // assert
        verify(errors, never()).rejectValue(eq("test"), anyString(), anyString());
    }
}