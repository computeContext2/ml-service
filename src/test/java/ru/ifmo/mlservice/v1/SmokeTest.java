package ru.ifmo.mlservice.v1;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.ifmo.mlservice.v1.controllers.LinearModelController;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SmokeTest {

    @Autowired
    @SuppressWarnings("unused")
    private LinearModelController linearModelController;

    @Test
    public void contextLoads() {
        assertThat(linearModelController).isNotNull();
    }
}
