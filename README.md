# Machine Learning Service
This is a simple ML service written in Java / Spring Boot.

The service provides the following features:
  - Model creation and training
  - Model evaluation
  - Prediction based on the previous trained model

### Tech
MLS uses a number of open source projects to work properly:
* [Spring Framework](https://spring.io) - is an application framework and inversion of control container for the Java platform
* [Weka](https://www.cs.waikato.ac.nz/ml/weka) - is a suite of machine learning software written in Java

### Build
MLS requires [JDK 11](http://www.oracle.com/technetwork/java/javase/downloads/index.html) and [Maven 3.6](https://maven.apache.org/download.cgi) to build.
To see UML diagrams in Javadocs, install [Graphviz](https://graphviz.gitlab.io/download).
Install the dependencies and execute the following commands:
```sh
$ cd ./ml-service
$ mvn clean package
```

### Run
To start the application, run the following commands:
```sh
$ cd ./target
$ java -jar ml-service-${project.version}.jar
```
Then open your favorite browser and follow the link for documentation http://localhost:8080/swagger-ui.html

Example of training data for the service:
```json
{
  "name": "ice_cream_sales",
  "attributes": [
    "temperature",
    "revenue"
  ],
  "train": [
    [  35.0, 12786.32 ],
    [  30.0, 12124.27 ],
    [  25.0, 8493.30  ],
    [  20.0, 5503.43  ],
    [  15.0, 3309.56  ],
    [  10.0, 2094.32  ],
    [   5.0, 998.98   ],
    [   0.0, 1240.10  ],
    [  -5.0, 1138.67  ],
    [ -10.0, 774.65   ],
    [ -15.0, 424.42   ],
    [ -20.0, 102.23   ],
    [ -25.0, 109.04   ],
    [ -30.0, 104.98   ],
    [ -35.0, 100.21   ]
  ],
  "test": [
    [  27.0, 9564.62  ],
    [   3.0, 587.37   ],
    [ -14.0, 123.78   ]
  ]
}
```

Also you can see and evaluate trained models on the http://localhost:8080/linear.html page.

License
----
The Unlicense
